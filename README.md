
# Jeu d'aventure texte

L'objectif de ce projet est de créer un [jeu d'aventure texte](https://fr.wikipedia.org/wiki/Jeu_vid%C3%A9o_textuel) en ligne de commandes. Le jeu doit décrire au joueur ce que son personnage voit et ce qui se passe autour de lui; le joueur doit entrer des commandes afin de se déplacer et d'agir sur son environnement.

## Mission 1: lieux et navigation

Avant toute chose, il faut que le jeu décrive au joueur où il se trouve, et qu'il puisse se déplacer d'un endroit à l'autre. Le déroulement du jeu pourrait ressembler à l'exemple suivant:

<details>
<summary>Exemple</summary>

> You are in the bedroom. West is the bathroom, north is the corridor.

`west`

> You are in the bathroom. East is the bedroom.

`west`

> You cannot go into that direction!

> You are in the bathroom. East is the bedroom.

`east`

> You are in the bedroom. West is the bathroom, north is the corridor.

</details>

Afin d'obtenir ce résultat, implémenter les classes ci-après en suivant les spécifications fournies.

### `Room`

- Représente un lieu dans lequel le joueur peut se trouver.

| Méthode | Description |
|---|---|
| _**String** getName()_ | Renvoie le nom du lieu (exemple: `"bedroom"`) |
| _**Room** getRoomInDirection(**Direction** direction)_ | Renvoie le lieu où l'on arrive lorsque l'on part de ce lieu et qu'on emprunte la direction passée en paramètre (exemple: depuis la chambre à coucher, en passant la direction ouest, on devrait obtenir la salle de bain) |

### `Direction`

- Représente une direction que le joueur peut emprunter pour se déplacer d'un lieu à l'autre.

| Méthode | Description |
|---|---|
| _**String** getName()_ | Renvoie le nom de la direction (exemple: `"north"`) |

### `RoomConnection`

- Représente un passage entre deux lieux.

| Méthode | Description |
|---|---|
| _**Room** getFromRoom()_ | Renvoie le lieu dont part le passage |
| _**Room** getToRoom()_ | Renvoie le lieu auquel le passage aboutit |
| _**Direction** getDirection()_ | Renvoie la direction qu'il faut suivre pour emprunter ce passage |

### `Game`

- Représente une partie jouée par le joueur.

| Méthode | Description |
|---|---|
| _**void** setup()_ | Initialise la partie en créant les objets de l'univers (les lieux et les directions) et en les associant les uns aux autres de la manière adéquate, et détermine le lieu de départ |
| _**void** update()_ | Décrit un cycle d'exécution de la partie: décrire le lieu courant, attendre une saisie de l'utilisateur, vérifier qu'elle correspond à une direction, changer de lieu si cette direction est empruntable depuis le lieu dans lequel on se trouve actuellement |
| _**boolean** isRunning()_ | Permet de savoir si la partie est en cours (`true`) ou si elle est terminée (`false`) |
| _**Room** getCurrentRoom()_ | Renvoie le lieu dans lequel le joueur se trouve actuellement |

### `App`

- Point d'entrée de l'application.

| Méthode | Description |
|---|---|
| _**static void** main(**String** args)_ | Processus principal. Crée une nouvelle partie et l'initialise, puis lui demande de réaliser un cycle d'exécution tant qu'elle est en cours. |

## Mission 2: objets et interactions

Maintenant que nos joueurs sont capables de se déplacer d'un lieu à une autre, il faudrait ajouter des éléments (objets, personnages, monstres…) avec lesquels ils pourront interagir.

<details>
<summary>Exemple</summary>

> You are in the bedroom. West is the bathroom, north is the corridor. There is a bed and a mirror.

`use bed`

> You take a quick nap. You feel refreshed!

`use mirror`

> You see your reflection. Looking good!

`open mirror`

> This does not open!

`talk to mirror`

> Silence...

`use toothbrush`

> There is no such item here!

</details>

### 1. Intégrer des objets à l'univers

- Écrire une classe `Item`, qui représente les éléments interactifs de l'univers.
- Chaque élément doit avoir un nom.
- Chaqué élément doit être visible ou non (c'est-à-dire que le joueur voit son nom affiché dans le jeu, et peut interagir avec, ou non).
- Chaque pièce peut contenir une quantité indéterminée d'éléments. La liste des éléments visibles doit être affichée automatiquement dans chaque pièce.

### 2. Interagir avec des éléments

- Écrire une classe `Command` qui représente une commande que l'utilisateur peut entrer dans la console.
- Chaque commande doit avoir un texte par défaut qui s'affichera si jamais l'utilisateur tente de l'utiliser avec un élément qui n'a pas été prévu pour (exemple: `talk to mirror`).
- Chaque élément peut réagir à un nombre indéterminé de commandes. Dans un premier temps, utiliser une commande particulière avec un élément particulier doit produire l'affichage d'un texte particulier.

### 3. Programmer des interactions complexes

Utiliser une commande sur un élément doit pouvoir produire une variété d'effets, dont afficher un texte n'est qu'un exemple.

Implémenter une ou plusieurs des classes suivantes:

| Classe | Description |
|---|---|
| **MessageEffect** | Produit l'affichage d'un message dans la console. |
| **EndGameEffect** | Termine la partie en cours. |
| **ChangeCurrentRoomEffect** | Change le lieu dans lequel le joueur se trouve actuellement. |
| **ChangeItemVisibilityEffect** | Change la visibilité d'un élément interactif. |
| **ModifyInventoryEffect** | Ajoute ou retire un élément interactif de l'inventaire du joueur. |

- Chaque élément peut réagir à chaque commande en utilisant l'un des effets proposés ci-dessus (au lieu de simplement afficher un message comme précédemment demandé).
- BONUS: Chaque élément peut réagir à chaque commande en utilisant une série d'effets, au lieu d'un seul effet.

#### Exemples d'interactions à implémenter

- Manger le biscuit sur la table de la cuisine (`eat cookie`) doit produire sa disparition de la pièce.
- Ouvrir le tiroir du bureau dans la chambre (`open drawer`) doit rendre visible un élément présent dans celui-ci (par exemple, un carnet de notes), le refermer (`close drawer`) doit le rendre invisible.
- Utiliser la voiture dans le garage (`use car`) doit produire le déplacement du joueur vers un autre lieu (par exemple, la ville). Utiliser la voiture dans ce dernier lieu doit produire le retour du joueur au garage.
- Ramasser une brosse à dents dans la salle de bain (`pick up toothbrush`) doit provoquer son ajout à l'inventaire et sa disparition de la pièce.
- Toucher une prise électrique (`touch plug`) doit produire la mort du héros, et donc la fin de la partie.

Si le bonus de l'étape 3 a été réalisé, chaque interaction doit être accompagnée d'au moins un message décrivant l'effet obtenu.

## Mission 3: Harmoniser les commandes

Le processus principal qui permet de faire fonctionner le jeu est désormais capable de reconnaître les saisies utilisateur qui correspondent à une direction (`east`, `south`, `west`…) ainsi que celles qui correspondent à une interaction avec un objet (`use bed`, `open drawer`, `pick up notepad`…). À ce stade, nous aimerions ajouter des commandes générales comme `help` qui pourrait afficher la liste des commandes possibles, ou encore `exit` qui permettrait d'interrompre le jeu. Cependant, nous commençons à entrevoir que le fait de rajouter des nouvelles commandes de la sorte risque de complexifier le processus principal du jeu, qui est déjà bien chargé: car si nous continuons sur notre lancée, chaque nouveau type de commandes va devoir être traité séparément des autres.

Dans un premier temps, considérant qu'il est de la responsabilité de chaque commande de savoir quel effet elle est censée produire, il pourrait être judicieux d'alléger le processus principal en déplaçant les différents effets possibles (quitter le jeu, afficher les commandes disponibles, changer de lieu, etc…) dans la classe correspondante.

De plus, considérant que les nouvelles commandes que nous souhaiterions implémenter, mais aussi les directions, et les actions que nous pouvons utiliser sur les éléments interactifs, sont finalement toutes des types de commandes qui ont simplement leur particularités, l'objectif ultime de cette mission est de parvenir à refactoriser le code de manière que tous les types de commandes soient traités de la mème manière, au lieu d'ètre traités séparément.

<details>
<summary>Illustration</summary>

La logique actuelle:
```java
class Game
{
    public void update()
    {
        // Attend une saisie utilisateur
        // Si la saisie utilisateur correspond à la commande "quitter le jeu"
            // Termine la partie
        // Si la saisie utilisateur correspond à la commande "afficher l'aide"
            // Affiche la liste des commandes
        // Si la saisie utilisateur correspond à une direction
            // Modifie le lieu actuel
        // Si la saisie utilisateur correspond à une interaction avec un élément présent dans le lieu actuel
            // Déclenche l'effet correspondant à la commande spécifiée sur cet élément
        // etc…
    }
}
```

La logique désirée:
```java
class Game
{
    public void update()
    {
        // Attend une saisie utilisateur
        // Pour chaque commande possible, peu importe son type réel (commande globale, direction, interaction…)
            // Demande à la commande de traiter la saisie utilisateur. Si la commande correspond à la saisie utilisateur, elle réalise l'effet de la commande par elle-même, et la boucle est interrompue. Sinon, rien ne se passe.
    }
}
```

</details>

### 1. Ajouter des commandes globales

- Implémenter une ou plusieurs des classes suivantes:

| Classe | Description |
|---|---|
| **ExitCommand** | Termine la partie en cours. |
| **HelpCommand** | Affiche la liste de toutes les commandes possibles. |
| **ResetCommand** | Recommence une nouvelle partie. |

> - Chacune de ces classes doit posséder une propriété **Game** qui fait référence à la partie en cours.
> - Chacune de ces classes doit posséder une méthode _**boolean** process(**String** userInput)_. Le rôle de cette méthode est de traiter une saisie utilisateur. Si la saisie utilisateur correspond à la commande concernée, alors elle doit produire l'effet de la commande et renvoyer **true**. Sinon, elle doit ne rien faire et renvoyer **false**.
- Ajouter au processus principal dans la classe **Game** une condition demandant à une instance de chacune de ces classes de traiter par elle-même la saisie utilisateur.

### 2. Refactoriser les directions

- Renommer la classe **Direction** en **DirectionCommand**.
- Adapter la classe **DirectionCommand** pour qu'elle corresponde aux mêmes spécifications que les commandes globales, énumérées au point 1.
- Adapter le processus principal dans la classe **Game** de façon que celui-ci se contente de demander à chaque direction de traiter par elle-même la saisie utilisateur.

### 3. Refactoriser les interactions avec les éléments

- Renommer la classe **Command** en **ItemCommand**.
- Adapter la classe **ItemCommand** pour qu'elle corresponde aux mêmes spécifications que les commandes globales, énumérées au point 1.
- Adapter le processus principal dans la classe **Game** de façon que celui-ci se contente de demander à chaque commande représentant une interaction avec un élément de traiter par elle-même la saisie utilisateur.

### 4. Unifier tous les types de commandes

- Écrire une interface **Command** qui synthétise la structure commune à toutes les classes de commandes crées précédemment. Toutes les classes de commandes doivent implémenter cette interface.
- Refactoriser le processus principal dans la classe **Game** de façon à rassembler tous les appels aux méthodes _process_ en un seul et même appel.
=======
# text-based-adventure



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/kevingrand1/text-based-adventure.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://gitlab.com/kevingrand1/text-based-adventure/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:45b924c82e7fcf302bc4f95755e86123?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

>>>>>>> README.md
